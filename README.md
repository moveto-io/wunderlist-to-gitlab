# wunderlist-to-gitlab

Import issues from Wunderlist to GitLab

Instructions:

* Export Wunderlist issues and download the wunderlist.json file
* Convert the wunderlist.json file to wunderlist.csv using wunderlist2csv
* (Optional) Delete all the existing GitLab issues using gitlab-delete-issues
* Import the wunderlist.csv issues to GitLab using gitlab-import-issues


Adapted from:

* https://github.com/tiangolo/wunderlist2csv
* https://github.com/grib0/gitlab-import-issues

