# deleteAll.py
# this will delete ALL existing gitlab issues, using a private token associated with a repo.

import os

# Fill in values here:
username = ""
projectname = ""
privateToken = ""
nIssues = 0

baseUrl = "https://gitlab.com/api/v4/projects/{}%2F{}/issues/".format(username, projectname)

for i in range(nIssues):
    s = """curl --request DELETE --header "PRIVATE-TOKEN: {}" {}{}""".format(privateToken, baseUrl, i)
    os.system(s)

