// import issues to gitlab from csv file

var querystring = require('querystring'),
  https = require('https'),
  fs = require('fs'),
  readline = require('readline'),
  csv = require("fast-csv"),
  randomColor = require('randomcolor');


// Update these values:
  
// Your gitlab host - do not include https://!
// var host = '10.82.136.217';
var host = 'gitlab.com';

// Your private key (see http://yourgitlabinstance/profile/personal_access_tokens)
// var privateToken = 'J_n6XdxJtw7j5Z3nwso8';
var privateToken = '';

// Your project id or group%2FprojectName
// var projectId = 'gribo%2Ftest-import';
var projectId = '';

// The name of the csv file to read
// var csvFileToRead = 'data/test.csv';
var csvFileToRead = 'data/wunderlist.csv';


// csv columns
var ISSUE_TITLE_INDEX = 0;
var ISSUE_LIST_INDEX = 1;
var ISSUE_DESCRIPTION_INDEX = 2;


function jsonToQueryString(json) {
  return Object.keys(json).map(function (key) {
    return encodeURIComponent(key) + '=' +
      encodeURIComponent(json[key]);
  }).join('&');
}


function performRequest(endpoint, method, data, success) {
  var dataString = JSON.stringify(data);
  // console.log(dataString);
  var headers = {};
  endpoint += '?private_token=' + privateToken + '&' + jsonToQueryString(data);
  console.log(host + endpoint);
  headers = {
    'Content-Type': 'application/json;charset=utf-8'
  };

  var options = {
    host: host,
    path: endpoint,
    method: method,
    headers: headers
  };

  var req = https.request(options, function (res) {
    res.setEncoding('utf-8');
    var responseString = '';

    res.on('data', function (data) {
      console.log('data received:', data);
      responseString += data;
    });

    res.on('end', function () {
      console.log('response string:', responseString);
      success();
    });

    res.on('error', function (error) {
      console.log(error);
    });
  });

  req.write(dataString);
  req.end();
}


function resumeCSVParsing() {
  csvstream.resume();
}


function logSetElements(value1, value2, set) {
  console.log('s[' + value1 + '] = ' + value2);
}

function createLabelIfNotExists(labelValue) {
  if (!labels.has(labelValue)) {
    console.log('Label ' + labelValue + ' does not exist');
    labels.add(labelValue);
    createLabel(labelValue);
  }
}

function createLabel(labelValue) {
  console.log('Creating label ' + labelValue);
  performRequest('/api/v4/projects/' + projectId + '/labels', 'POST', {
    name: labelValue,
    color: randomColor()
  }, function (data) {
    console.log('fetched:', data);
  });
}

function createIssue(title, labels, description) {
  performRequest('/api/v4/projects/' + projectId + '/issues', 'POST', {
    title: title,
    labels: labels,
    description: description
  }, function (data) {
    // keep csv parsing
    // we need to pause otherwise we get https://gitlab.com/gitlab-org/gitlab-ee/issues/2374
    setTimeout(resumeCSVParsing, 3000);
  });
}


// // Display all issues to test host + projectId + private key
// console.log('display issues');
// function getIssues() {
//   performRequest('/api/v4/projects/' + projectId + '/issues', 'GET', {
//     state: "opened"
//   }, function (data) {
//     console.log('fetched:', data);
//   });
// }
// getIssues();

var i = 0;
var labels = new Set();

var csvstream = csv.fromPath(csvFileToRead, { delimiter: ',', header: true })
  .on("data", function (columns) {

    // console.log('Parsing line : ' + columns);

    // Remove variable that are not required
    var title = columns[ISSUE_TITLE_INDEX];
    var list = columns[ISSUE_LIST_INDEX];
    var description = columns[ISSUE_DESCRIPTION_INDEX];

    // split out label from title if there
    // (I had items organized like "#ui - add blog")
    if (title.slice(0,1) === '#') {
      title = title.slice(1);
    }
    title = title.replace(' - @', ' - ');
    console.log(list, title);

    if (i > 0) {
      createLabelIfNotExists(list);
    }

    // We create labels for all this variable
    var labelArray = [list];

    // remove empty labels
    var filteredArr = labelArray.filter(function (val) {
      return !(val === "" || typeof val == "undefined" || val === null);
    });

    // Keep new lines in the issue
    description = description.split(/\r?\n/);
    description = description.join('\n\n');

    if (i > 0) {
      // we need to pause otherwise we get https://gitlab.com/gitlab-org/gitlab-ee/issues/2374
      csvstream.pause();
      createIssue(title, filteredArr.join(), description);
    }

    i++;
  })

  .on("end", function () {
    console.log("CSV file parsing done, found Labels :");
    // Display labels to create them manually before importing issue
    labels.forEach(logSetElements);
  })
  
  .on("error", function (error) {
    console.log(error);
  });
